package com.mad.palmtest.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Scroller;

/**
 * swipemenu view group
 */
public class SwipeMenu extends ViewGroup {
    private final static String TAG = "MAD_SwipeMenu";

    private int downX, moveX, moved;
    private Scroller mScroller = new Scroller(getContext());
    //private onScrollStateChanged onScrollStateChanged;
    private boolean haveShowRight =false;

    public static SwipeMenu mSwipeMenu;

    public SwipeMenu(Context context) {
        super(context);
    }

    public SwipeMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwipeMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mSwipeMenu != null && mSwipeMenu == this) {
            mSwipeMenu.closeMenu_();
            mSwipeMenu = null;
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int cCount = getChildCount();

        for (int i = 0; i < cCount; i++) {
            View child = getChildAt(i);

            if (i == 0) {
                child.layout(l, t, r, b);
            } else if (i == 1) {
                child.layout(r, t, r + child.getMeasuredWidth(), b);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        measureChildren(widthMeasureSpec, heightMeasureSpec);

        int width = MeasureSpec.getSize(widthMeasureSpec);
        View child = getChildAt(0);
        int margin =
                ((MarginLayoutParams) child.getLayoutParams()).topMargin +
                        ((MarginLayoutParams) child.getLayoutParams()).bottomMargin;
        setMeasuredDimension(width, getChildAt(0).getMeasuredHeight() + margin);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }


    /** move to appointed position */
    private void moveTo(int destX) {
        int scrollX = getScrollX();
        int delta = destX - scrollX;
        // move X during 100s
        mScroller.startScroll(scrollX, 0, delta, 0, 500);
        invalidate();
    }

    /** close menu, move to origin */
    public void closeMenu_() {
        moveTo(0);
        haveShowRight = false;
    }

    public static void closeMenu() {
        mSwipeMenu.closeMenu_();
    }

    @Override
    public void computeScroll() {
        if (mScroller.computeScrollOffset()) {
            scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
            postInvalidate();
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Log.d(TAG, "onInterceptTouchEvent: ACTION_DOWN");
                downX = (int) ev.getRawX();
                break;
            case MotionEvent.ACTION_MOVE:
                Log.d(TAG, "onInterceptTouchEvent: ACTION_MOVE");
                moveX = (int) ev.getRawX();
                if (moveX - downX > 2) {
                    return true;
                }
                break;
            case MotionEvent.ACTION_UP:
                Log.d(TAG, "onInterceptTouchEvent: ACTION_UP");
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!mScroller.isFinished()) {
            return false;
        }
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = (int) ev.getRawX();
                break;

            case MotionEvent.ACTION_MOVE:
                if (mSwipeMenu != null && mSwipeMenu == this && haveShowRight) {
                    closeMenu();
                    return true;
                }
                moveX = (int) ev.getRawX();

                moved = moveX - downX;

                if (haveShowRight) {
                    moved -= getChildAt(1).getMeasuredWidth();
                }
                scrollTo(-moved, 0);
                if (getScrollX() <= 0) {
                    scrollTo(0, 0);
                } else if (getScrollX() >= getChildAt(1).getMeasuredWidth()) {
                    scrollTo(getChildAt(1).getMeasuredWidth(), 0);
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.d(TAG, "onTouchEvent: ACTION_CANCEL");
                if (getScrollX() >= getChildAt(1).getMeasuredWidth() / 10) {
                    Log.d(TAG, "onTouchEvent: 1");
                    haveShowRight = true;
                    mSwipeMenu = this;
                    moveTo(getChildAt(1).getMeasuredWidth());
                }

            case MotionEvent.ACTION_UP:
                Log.d(TAG, "onTouchEvent: ACTION_UP");
                if (mSwipeMenu != null) {
                    closeMenu();
                }
                if (getScrollX() >= getChildAt(1).getMeasuredWidth() / 10) {
                    Log.d(TAG, "onTouchEvent: 1");
                    haveShowRight = true;
                    mSwipeMenu = this;
                    moveTo(getChildAt(1).getMeasuredWidth());
                } else {
                    Log.d(TAG, "onTouchEvent: 2");
                    haveShowRight = false;
                    moveTo(0);
                }
                break;
        }
        return true;
    }
}
