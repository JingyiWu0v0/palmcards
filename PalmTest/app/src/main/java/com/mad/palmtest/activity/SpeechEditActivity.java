package com.mad.palmtest.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


import com.mad.palmtest.DateFormater;
import com.mad.palmtest.R;
import com.mad.palmtest.javabean.Speech;
import com.mad.palmtest.knife.KnifeText;
import com.mad.palmtest.datamanager.SpeechDatabaseHelper;

import java.util.Date;

/**
 * The Editor for full text speech
 * allow user edit it with different spans
 */
public class SpeechEditActivity extends AppCompatActivity {
    private final static String TAG = "MAD_EDIT_FULLTEXT";
    private static final String SPEECH_ID = "speech_id";

    private ImageButton mToolUndo;
    private ImageButton mToolRedo;
    private ImageButton mToolHighlight;
    private ImageButton mToolBold;
    private ImageButton mToolItalic;
    private ImageButton mToolUnderline;
    private ImageButton mToolLink;
    private ImageButton mToolBullet;
    private ImageButton mToolStrikethrough;
    private Toolbar toolbar;

    private KnifeText mSpeechEditor;

    private SQLiteOpenHelper mSpeechDbHelper;                                    // for help get database, and do some actions to database
    private SQLiteDatabase mSpeechDatabase;                                      // speech's database

    private Speech mSpeech;

    private int mYear;
    private int mMonth;
    private int mDay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speech_edit);

        mToolUndo = (ImageButton)findViewById(R.id.tool_undo_btn);
        mToolRedo = (ImageButton)findViewById(R.id.tool_redo_btn);
        mToolHighlight = (ImageButton)findViewById(R.id.tool_highlight_btn);
        mToolBold = (ImageButton)findViewById(R.id.tool_bold_btn);
        mToolItalic = (ImageButton) findViewById(R.id.tool_italic_btn);
        mToolUnderline  = (ImageButton) findViewById(R.id.tool_underline_btn);
        mToolStrikethrough = (ImageButton)findViewById(R.id.tool_strikethrough_btn);
        mToolBullet = (ImageButton)findViewById(R.id.tool_bullet_btn);
        mToolLink = (ImageButton)findViewById(R.id.tool_link_btn);
        mSpeechEditor = (KnifeText) findViewById(R.id.speech_editor);

        /* set up tool bar */
        toolbar = (Toolbar) findViewById(R.id.speech_editor_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle(R.string.unname);

        /* set up for get database */
        mSpeechDbHelper = SpeechDatabaseHelper.getInstance(SpeechEditActivity.this.getApplicationContext());
        mSpeechDatabase = mSpeechDbHelper.getWritableDatabase();


        /** get speech from database by it's id */
        Intent intent = getIntent();
        if (intent!= null && intent.getIntExtra(SPEECH_ID,0)!= 0){
            mSpeech = SpeechDatabaseHelper.getSpeech(mSpeechDatabase,
                    intent.getIntExtra(SPEECH_ID,0));
            mSpeechEditor.fromHtml(mSpeech.getSpeechDoc());
        }

        setUpToolComponents();

    }

    /* setup all tools */
    private void setUpToolComponents(){

        /** undo action*/
        mToolUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.undo();
            }
        });


        /** redo action*/
        mToolRedo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.redo();
            }
        });


        /** Add style of highlight to text  and set it is keyword */
        mToolHighlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.highlight(!mSpeechEditor.contains(KnifeText.FORMAT_HIGHLIGHT));
            }
        });

        /** Add style of bold to text*/
        mToolBold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.bold(!mSpeechEditor.contains(KnifeText.FORMAT_BOLD));
            }
        });

        /** Add style of italic to text*/
        mToolItalic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.italic(!mSpeechEditor.contains(KnifeText.FORMAT_ITALIC));
            }
        });

        /** Add style of underline to text*/
        mToolUnderline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.underline(!mSpeechEditor.contains(KnifeText.FORMAT_UNDERLINED));
            }
        });

        /** Add style of strike through to text*/
        mToolStrikethrough.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.strikethrough(!mSpeechEditor.contains(KnifeText.FORMAT_STRIKETHROUGH));
            }
        });

        /** Add style of bullet to text*/
        mToolBullet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.bullet(!mSpeechEditor.contains(KnifeText.FORMAT_BULLET));
            }
        });

        /** Add style of link to text*/
        mToolLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.underline(!mSpeechEditor.contains(KnifeText.FORMAT_LINK));
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.speech_editor_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_save) {
            //quick save speech if already saved
            saveSpeech();
            return true;
        }else if (id == R.id.menu_saveas){
            // save speech as another file
            saveAsSpeech();
            return true;
        } else if (id == R.id.menu_clear){
            // clean all formats
            mSpeechEditor.clearFormats();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** quick save speech if already saved */
    private void saveSpeech(){
        if (mSpeech!=null){
            mSpeech.setSpeechDoc(mSpeechEditor.toHtml());
            SpeechDatabaseHelper.saveSpeech(mSpeechDatabase,mSpeech);
            return;
        } else saveAsSpeech();      // if not exist, save as another file
    }

    /**
     * pop window of speech detail, to save speech as another file
     */
    private void saveAsSpeech(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View popWin = LayoutInflater.from(this).inflate(R.layout.pop_saving_detail, null);
        builder.setView(popWin);
        final AlertDialog dialog = builder.show();

        final EditText mTitle = (EditText)popWin.findViewById(R.id.pop_save_title);
        final TextView mDate = (TextView)popWin.findViewById(R.id.pop_save_date);
        Button popSaveBtn = (Button)popWin.findViewById(R.id.pop_save_yes);
        Button popCancelBtn = (Button)popWin.findViewById(R.id.pop_save_cancel);

        mDate.setOnClickListener(new View.OnClickListener() {
            // choose the date want to speech
            @Override
            public void onClick(View v) {
                if (mSpeech==null){
                    Date today = new Date(System.currentTimeMillis());
                    mYear = today.getYear();
                    mMonth = today.getMonth();
                    mDay = today.getDay();
                }else {
                    Date date = mSpeech.getSpeechTime();
                    mYear = date.getYear();
                    mMonth = date.getMonth();
                    mDay = date.getDay();
                }
                new DatePickerDialog(SpeechEditActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        setTitle((monthOfYear+1) + "-" + dayOfMonth);
                        mYear = year;
                        mMonth = monthOfYear;
                        mDay = dayOfMonth;
                        mDate.setText((monthOfYear+1) +"-"+dayOfMonth);

                    }
                },mYear, mMonth,mDay).show();
            }
        });
        popSaveBtn.setOnClickListener(new View.OnClickListener() {
            // save speech to database
            @Override
            public void onClick(View v) {
                mSpeech = new Speech(0, mTitle.getText().toString(),
                        DateFormater.intToDate(mYear, mMonth, mDay),
                        mSpeechEditor.toHtml());
                SpeechDatabaseHelper.saveSpeech(mSpeechDatabase,mSpeech);
                dialog.dismiss();
            }
        });

        popCancelBtn.setOnClickListener(new View.OnClickListener() {
            // cancel the saving
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    /*Called when activity becomes visible to the user*/
    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
    }

    /*Called when activity starts interacting with user*/
    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
        if(mSpeech!=null){
            toolbar.setTitle(mSpeech.getSpeechTitle());
        }else {
            toolbar.setTitle(getResources().getString(R.string.unname));
        }

    }

    /*Called when current activity is being paused and the previous activity is being resumed*/
    @Override
    protected void onPause() {
        Log.d(TAG, "onPause: ");
        super.onPause();
    }

    /*Called when activity is no longer visible to user*/
    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
    }

    /*Called before the activity is destroyed by the system (either manually or by the system to conserve memory) */
    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }

}
