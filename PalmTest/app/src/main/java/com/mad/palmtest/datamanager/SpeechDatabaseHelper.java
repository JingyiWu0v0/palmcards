package com.mad.palmtest.datamanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mad.palmtest.DateFormater;
import com.mad.palmtest.R;
import com.mad.palmtest.javabean.PointSpeech;
import com.mad.palmtest.javabean.Speech;
import com.mad.palmtest.javabean.SpeechsInfo;

import java.util.ArrayList;
import java.util.Date;

/**
 * the sql database helper
 * helpe app to take or saving datas in sql
 *
 */
public class SpeechDatabaseHelper extends SQLiteOpenHelper {
    private final static String TAG = "MAD_SQL";

    /* names for database and table */
    private static final String DATABASE_NAME = "PalmCard";
    private static final int DATABASE_VERSION = 1;

    private static final String ORDER_TYPE = " DESC";
    private static final String SPEECH_TABLE = "speechs";
    private static final String SPEECH_TITLE = "speech_title";
    private static final String SPEECH_EDITDATE = "speech_editdate";
    private static final String SPEECH_DATE = "speech_date";
    private static final String SPEECH_TYPE = "speech_type";

    private static final String FULLTEXT_TABLE = "fulltexts";
    private static final String FULLTEXT_DOC = "fulltext_doc";
    private static final String FULLTEXT_KEY = "fulltext_key";

    private static final String POINT_TABLE = "points";
    private static final String POINT_TITLE = "point_title";
    private static final String POINT_TIME = "point_time";
    private static final String POINT_DOC = "point_doc";
    private static final String POINT_PAGE = "point_page";

    private static final String TYPE_FULLTEXT = "fulltext";
    private static final String TYPE_POINT = "point";

    private static final String BOLD = "<b>Bold</b><br><br>";
    private static final String ITALIT = "<i>Italic</i><br><br>";
    private static final String UNDERLINE = "<u>Underline</u><br><br>";
    private static final String STRIKETHROUGH = "<s>Strikethrough</s><br><br>"; // <s> or <strike> or <del>
    private static final String BULLET = "<ul><li>asdfg</li></ul>";
    private static final String QUOTE = "<blockquote>Quote</blockquote>";
    private static final String LINK = "<a href=\"https://github.com/mthli/Knife\">Link</a><br><br>";
    private static final String EXAMPLE = BOLD + ITALIT + UNDERLINE + STRIKETHROUGH + BULLET + QUOTE + LINK;

    private static SpeechDatabaseHelper sInstance;
    private String sql;
    private ContentValues contentValues;

    private Context mContext;

    /**
     * For out side get databaseï¼Œmake sure only one database is exist
     *
     * @param context the context of application activity
     * @return SpeechDatabaseHelper of speech database
     */
    public static synchronized SpeechDatabaseHelper getInstance(Context context) {
        Log.d(TAG, "getInstance: ");

        // make sure only one database exist
        if (sInstance == null) {
            // TODO command it after test
            //context.deleteDatabase(DATABASE_NAME);

            sInstance = new SpeechDatabaseHelper(context.getApplicationContext());
            Log.d(TAG, "getInstance: created");
        }
        return sInstance;
    }

    /**
     * private constructor, only allows outside create database by getInstance()
     *
     * @param context the context of application activity
     */
    private SpeechDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "SpeechDatabaseHelper: ");
        mContext = context;
    }

    /**
     * only execute when first time create database
     * create basic table and insert default data
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate: database");

        // create speech info sql table
        sql = "create table " + SPEECH_TABLE +
                "( id INTEGER primary key autoincrement, " +
                SPEECH_TITLE + " VARCHAR2(30), " +
                SPEECH_EDITDATE + " DATETIME, " +
                SPEECH_DATE + " DATE, " +
                SPEECH_TYPE + " VARCHAR2(10)) ";
        db.execSQL(sql);

        // create fulltexts type doc sql table
        sql = "create table " + FULLTEXT_TABLE +
                "( id INTEGER primary key, " +
                FULLTEXT_DOC + " VARCHAR2)";
        db.execSQL(sql);

        // create point type doc sql table
        sql = "create table " + POINT_TABLE +
                "( point_id INTEGER primary key autoincrement, id INTEGER, " +
                POINT_TITLE + " VARCHAR2(30), " +
                POINT_TIME + " INTGER(30), " +
                POINT_PAGE + " INTGER(30), " +
                POINT_DOC + " VARCHAR2) ";

        db.execSQL(sql);

        // set up example data
        contentValues = new ContentValues();
        contentValues.put(SPEECH_TITLE,"Eg. of Full Speech");
        contentValues.put(SPEECH_EDITDATE,"2000-01-01 00:00:00");
        contentValues.put(SPEECH_DATE,"2000-01-01");
        contentValues.put(SPEECH_TYPE,"fulltext");
        db.insert(SPEECH_TABLE,null, contentValues);

        contentValues = new ContentValues();
        contentValues.put("id",1);
        contentValues.put(FULLTEXT_DOC,mContext.getResources().getString(R.string.fulltext_exp_01));
        db.insert(FULLTEXT_TABLE,null, contentValues);

        contentValues = new ContentValues();
        contentValues.put(SPEECH_TITLE,"Eg. of Point Speech");
        contentValues.put(SPEECH_EDITDATE,"2000-01-01 00:00:00");
        contentValues.put(SPEECH_DATE,"2000-01-01");
        contentValues.put(SPEECH_TYPE,"point");
        db.insert(SPEECH_TABLE,null, contentValues);
        contentValues = new ContentValues();

        contentValues = new ContentValues();
        contentValues.put("id",2);
        contentValues.put(POINT_TITLE,mContext.getResources().getString(R.string.point_tit_01));
        contentValues.put(POINT_TIME,1);
        contentValues.put(POINT_PAGE,1);
        contentValues.put(POINT_DOC,mContext.getResources().getString(R.string.point_exp_01));
        db.insert(POINT_TABLE,null, contentValues);

        contentValues = new ContentValues();
        contentValues.put("id",2);
        contentValues.put(POINT_TITLE,mContext.getResources().getString(R.string.point_tit_02));
        contentValues.put(POINT_TIME,3);
        contentValues.put(POINT_PAGE,2);
        contentValues.put(POINT_DOC,mContext.getResources().getString(R.string.point_exp_02));
        db.insert(POINT_TABLE,null, contentValues);

        contentValues = new ContentValues();
        contentValues.put("id",2);
        contentValues.put(POINT_TITLE,mContext.getResources().getString(R.string.point_tit_03));
        contentValues.put(POINT_TIME,5);
        contentValues.put(POINT_PAGE,3);
        contentValues.put(POINT_DOC,mContext.getResources().getString(R.string.point_exp_03));
        db.insert(POINT_TABLE,null, contentValues);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * public function for get all speech's data from database
     *
     * @param db the database need get data
     * @return speechList from database
     */
    public static ArrayList<SpeechsInfo> getAllSpeechInfo(SQLiteDatabase db){
        ArrayList<SpeechsInfo> speechList = new ArrayList<>();
        SpeechsInfo speechinfo;

        Cursor cursor = db.query(
                SPEECH_TABLE, new String[]{"id", SPEECH_TITLE,SPEECH_EDITDATE,SPEECH_DATE,SPEECH_TYPE},
                null , null,
                null, null, SPEECH_EDITDATE + ORDER_TYPE
        );

        while (cursor.moveToNext()){
            speechinfo = new SpeechsInfo(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex(SPEECH_TITLE)),
                    DateFormater.formatSQLDate(cursor.getString(cursor.getColumnIndex(SPEECH_EDITDATE)),true),
                    DateFormater.formatSQLDate(cursor.getString(cursor.getColumnIndex(SPEECH_DATE)),false),
                    cursor.getString(cursor.getColumnIndex(SPEECH_TYPE))
                    );
            speechList.add(speechinfo);
        }

        cursor.close();
        return speechList;
    }

    /**
     * Get fulltext speech by it's ID
     * @param ID the id of speech in sql table
     */
    public static Speech getSpeech(SQLiteDatabase db, int ID){
        Cursor cursor = db.rawQuery("select s.id, s."+SPEECH_TITLE+", s."+SPEECH_DATE+", f."+ FULLTEXT_DOC +
                        " from "+ SPEECH_TABLE +" s, "+ FULLTEXT_TABLE +" f " +
                        " where s.id = " + ID +
                        " group by s.id = f.id " +
                        " order by s." + SPEECH_EDITDATE + ORDER_TYPE,
                new String[]{});

        Speech speech = null;
        while (cursor.moveToNext()){
             speech = new Speech(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex(SPEECH_TITLE)),
                    DateFormater.toJavaDate(cursor.getString(cursor.getColumnIndex(SPEECH_DATE)),false),
                    cursor.getString(cursor.getColumnIndex(FULLTEXT_DOC))
            );
        }
        cursor.close();
        return speech;
    }

    /**
     * Get point speech by it's ID
     * @param ID the identifier of speech in sql table
     */
    public static PointSpeech getPointSpeech(SQLiteDatabase db, int ID){
        Cursor cursor;
        cursor = db.query(
                SPEECH_TABLE, new String[]{"id", SPEECH_TITLE,SPEECH_DATE},
                "id = "+ID , null,
                null, null, SPEECH_EDITDATE + ORDER_TYPE
        );
        cursor.moveToFirst();
        PointSpeech point = new PointSpeech(
                ID,
                DateFormater.toJavaDate(cursor.getString(cursor.getColumnIndex(SPEECH_DATE)),false),
                cursor.getString(cursor.getColumnIndex(SPEECH_TITLE))
        );

        cursor = db.rawQuery("select p.id, p."+ POINT_PAGE + ", p."+ POINT_TITLE + ", p."+ POINT_TIME + ", p."+ POINT_DOC +
                        " from "+ POINT_TABLE +" p " +
                        " where p.id = " + ID +
                        " order by p." + POINT_PAGE,
                new String[]{});

        while (cursor.moveToNext()){
            point.addPageTitles(cursor.getString(cursor.getColumnIndex(POINT_TITLE)));
            point.addPageTimes(cursor.getInt(cursor.getColumnIndex(POINT_TIME)));
            point.addPages(cursor.getString(cursor.getColumnIndex(POINT_DOC)));
        }
        cursor.close();
        return point;
    }

    /**
     * Save fulltext speech by it's ID or add new Speech
     * @param speech the object include speech's title, date, texts...
     */
    public static void saveSpeech(SQLiteDatabase db, Speech speech){
        // get current date as last edit date
        String currDate = DateFormater.toSQLDateTime(new Date(System.currentTimeMillis()));

        // set value
        ContentValues infoContent = new ContentValues();
        infoContent.put(SPEECH_TITLE,speech.getSpeechTitle());
        infoContent.put(SPEECH_DATE,DateFormater.toSQLDate(speech.getSpeechTime()));
        infoContent.put(SPEECH_EDITDATE,currDate);
        infoContent.put(SPEECH_TYPE,TYPE_FULLTEXT);

        ContentValues textContent = new ContentValues();
        textContent.put(FULLTEXT_DOC,speech.getSpeechDoc());
        Log.d(TAG, "saveSpeech: "+ speech.getSpeechDoc());


        if(speech.getSpeechId() != 0){
            db.update(SPEECH_TABLE, infoContent,"id = "+ speech.getSpeechId(),null);
            db.update(FULLTEXT_TABLE, textContent,"id = "+ speech.getSpeechId(),null);

        }else {
            db.insert(SPEECH_TABLE,null,infoContent);

            Cursor cursor = db.rawQuery("select LAST_INSERT_ROWID() ",null);
            cursor.moveToFirst();
            int id = cursor.getInt(0);
            textContent.put("id",id);
            db.insert(FULLTEXT_TABLE,null,textContent);
            cursor.close();
        }
    }

    /**
     * Save Point speech by it's ID or add new {}oint Speech
     * @param speech the object include speech's title, date, texts...
     */
    public static void savePointSpeech(SQLiteDatabase db, PointSpeech speech){
        // get current date as last edit date
        String currDate = DateFormater.toSQLDateTime(new Date(System.currentTimeMillis()));

        // set value for info table
        ContentValues infoContent = new ContentValues();
        infoContent.put(SPEECH_TITLE,speech.getSpeechTitle());
        infoContent.put(SPEECH_DATE,DateFormater.toSQLDate(speech.getSpeechDate()));
        infoContent.put(SPEECH_EDITDATE,currDate);
        infoContent.put(SPEECH_TYPE,TYPE_POINT);

        // save info and get id relate to other table
        int saveId;
        if(speech.getSpeechId() != 0){
            db.update(SPEECH_TABLE, infoContent,"id = "+ speech.getSpeechId(),null);
            db.delete(POINT_TABLE,"id = "+ speech.getSpeechId(),null);
            saveId = speech.getSpeechId();

        }else {
            db.insert(SPEECH_TABLE,null,infoContent);
            Cursor cursor = db.rawQuery("select LAST_INSERT_ROWID() ",null);
            cursor.moveToFirst();
            saveId = cursor.getInt(0);
            cursor.close();
        }

        /* get list of value and insert into different page */
        ArrayList<String>   mPageTitles = speech.getPageTitles();
        ArrayList<Integer>  mPageTimes = speech.getPageTimes();
        ArrayList<String>   mPageDocs = speech.getPages();
        for (int i = 0;i< mPageTitles.size();i++){
            ContentValues textContent = new ContentValues();
            textContent.put("id",saveId);
            textContent.put(POINT_TITLE,mPageTitles.get(i));
            textContent.put(POINT_TIME,mPageTimes.get(i));
            textContent.put(POINT_DOC,mPageDocs.get(i));
            textContent.put(POINT_PAGE,i);
            db.insert(POINT_TABLE,null,textContent);
        }
        Log.d(TAG, "saveSpeech: "+ mPageDocs.get(0));

    }
    /**
     * delete fulltext speech by it's ID
     * @param ID
     * @param type the type of this speech doc
     */
    public static void deleteSpeech(int ID, String type){
        SQLiteDatabase db = sInstance.getWritableDatabase();
        db.delete(SPEECH_TABLE,"id = " + ID, new String[]{});
        if (type.equals(TYPE_FULLTEXT)){
            db.delete(FULLTEXT_TABLE,"id = " + ID, new String[]{});
        }else {
            db.delete(POINT_TABLE,"id = " + ID, new String[]{});
        }
    }
}
