package com.mad.palmtest.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mad.palmtest.R;

import java.util.ArrayList;

public class PageAdapter extends RecyclerView.Adapter<PageAdapter.ViewHolder> {
    private ArrayList<String> mPagesList;
    private Context mContext;
    private int mCurrentPage = 0;
    private OnItemClickListener mOnItemClickListener;

    public PageAdapter(ArrayList<String> mPagesList, Context context) {
        this.mPagesList = mPagesList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pages_row, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.mPageNum.setText(String.valueOf(position));
        holder.mTitle.setText(mPagesList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onClick(position);
                mCurrentPage = position;
            }
        });
        if (position ==mCurrentPage){
            holder.mTitle.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        }else {
            holder.mTitle.setTextColor(mContext.getResources().getColor(R.color.black_overlay));
        }
    }

    public interface OnItemClickListener{
        void onClick( int position);
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener ){
        this.mOnItemClickListener=onItemClickListener;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int mCurrentPage) {
        this.mCurrentPage = mCurrentPage;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mPagesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mPageNum;
        ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.adp_page_title);
            mPageNum = itemView.findViewById(R.id.adp_page_num);
            itemView.setTag(this);
        }
    }
}
