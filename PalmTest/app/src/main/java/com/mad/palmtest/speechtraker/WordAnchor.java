package com.mad.palmtest.speechtraker;

/**
 * word anchor for carry each word's info
 *
 */
public class WordAnchor {
    private String word;
    private int anchor;
    private int start;
    private int end;

    public WordAnchor(String word, int anchor, int start, int end) {
        this.word = word;
        this.anchor = anchor;
        this.start = start;
        this.end = end;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getAnchor() {
        return anchor;
    }

    public void setAnchor(int anchor) {
        this.anchor = anchor;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }
}
