package com.mad.palmtest.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mad.palmtest.DateFormater;
import com.mad.palmtest.R;
import com.mad.palmtest.javabean.Speech;
import com.mad.palmtest.javabean.SpeechsInfo;
import com.mad.palmtest.knife.KnifeParser;
import com.mad.palmtest.knife.KnifeText;
import com.mad.palmtest.datamanager.SettingHelper;
import com.mad.palmtest.datamanager.SpeechDatabaseHelper;
import com.mad.palmtest.speechtraker.AnchorHelper;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * the speech active page
 * it allows app to tracing speech
 */
public class FulltextSpeechActivity extends AppCompatActivity {
    private static final String TAG = "MAD";
    private static final String SPEECH_ID = "speech_id";
    private final static String USEDHAND = "used_hand";
    private final static String TEXTSIZE = "text_size";
    private final static String ANCHOR_START  = "start";
    private final static String ANCHOR_END  = "end";

    private final static String PERMISIONS[] = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.MODIFY_AUDIO_SETTINGS,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.RECEIVE_BOOT_COMPLETED
    };

    private RelativeLayout mSpeechLayout;
    private RelativeLayout mRightInner;
    private TextView mSpeechText;
    private TextView mTimerTotalTv;
    private TextView mKeywordTv;
    private Button mPageStart;
    private Button mPageEnd;
    private ViewFlipper mControlBox;
    private ProgressBar mLoadingAnchors;

    /* calculate time */
    private Handler mTimeHandler;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private static int mTotalTime;
    private ImageButton mTimerPause;

    /* recognize speech */
    private SpeechRecognizer mRecognizer;
    private AudioManager mAudioManager;
    //private Intent speechIntent;
    private AnchorHelper mAnchorHelper;
    private Map<String, Integer> currentSentence;
    private BackgroundColorSpan mHighlightCurrent = new BackgroundColorSpan(Color.YELLOW);
    private BackgroundColorSpan mHighlightPotential = new BackgroundColorSpan(Color.YELLOW);
    private SpannableStringBuilder mBuilder;
    private int mStart = -1;
    private int mEnd = -1;
    private static boolean isPause = false;
    private static boolean isListening = false;

    private SQLiteOpenHelper mSpeechDbHelper;                                    // for help get database, and do some actions to database
    private SQLiteDatabase mSpeechDatabase;                                      // speech's database

    /* customer settings */
    SettingHelper mSettingHelper;
    Map<String,String> settings;
    private int mTextSize;

    Speech mFullTextSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fulltext_speech);
        getWindow().setFlags(WindowManager.LayoutParams. FLAG_FULLSCREEN ,WindowManager.LayoutParams. FLAG_FULLSCREEN);
        Log.d(TAG, "onCreate: ");


        // get components
        mRightInner = findViewById(R.id.dem_right_inside);
        mControlBox = findViewById(R.id.timerViewSwitcher);
        mSpeechText = findViewById(R.id.fulltext_body);
        mKeywordTv = findViewById(R.id.dem_keyword);
        mSpeechLayout = findViewById(R.id.fulltext_layout);
        mPageStart = findViewById(R.id.dem_page_start);
        mPageEnd = findViewById(R.id.dem_page_end);
        mTimerTotalTv = findViewById(R.id.dem_timer_total);
        mTimerPause = findViewById(R.id.dem_timer_pause);
        mLoadingAnchors = findViewById(R.id.dem_loading_progress);

        /* set up for get database */
        mSpeechDbHelper = SpeechDatabaseHelper.getInstance(FulltextSpeechActivity.this.getApplicationContext());
        mSpeechDatabase = mSpeechDbHelper.getReadableDatabase();

        // setup the speech
        setupCustomerSetting();
        setupSpeech();
        setupControlBox();
        setupSpeechTraker();

    }

    /** speech tracing componenets*/
    private void setupSpeechTraker(){
        getPermission();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM,true);

        mAnchorHelper = AnchorHelper.getInstance(this);
        new  initSpeechAnchors().execute();
        mRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {
                Log.d(TAG, "onReadyForSpeech: ---");
            }

            @Override
            public void onBeginningOfSpeech() {
                Log.d(TAG, "onBeginningOfSpeech: ---------------------");
            }

            @Override
            public void onRmsChanged(float rmsdB) { }

            @Override
            public void onBufferReceived(byte[] buffer) { }

            @Override
            public void onEndOfSpeech() {
                Log.d(TAG, "onEndOfSpeech: ---------------------------");
            }

            @Override
            public void onError(int error) {
                Log.d(TAG, "onError: xxx "+ getErrorMsg(error));
                mRecognizer.cancel();
                isListening = false;
                promptSpeechInput();
            }

            @Override
            public void onResults(Bundle results) {
                mRecognizer.stopListening();    //pause listening wait for result
                ArrayList<String> nbest = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                assert nbest != null;
                if (nbest.size() > 0) {
                    Log.d(TAG, "onResults:----> " + nbest.get(0));
                    currentSentence = mAnchorHelper.getCurrentSentence(nbest.get(0));
                    if (currentSentence.get(ANCHOR_START)==mStart)return;
                    mStart = currentSentence.get(ANCHOR_START);
                    mEnd = currentSentence.get(ANCHOR_END);
                    mBuilder.setSpan(mHighlightCurrent,mStart,mEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mSpeechText.setText(mBuilder);
                }
                if (!isPause){
                    Log.d(TAG, "onResults: start again");
                    mRecognizer.cancel();
                    if (!isListening) promptSpeechInput();    // start recognize again after result out
                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {
                ArrayList<String> nbest = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                assert nbest != null;
                if (nbest.size() > 0) {
                    //Log.d(TAG, "onPartialResults: " + nbest.get(0));
                    currentSentence = mAnchorHelper.getPotentialSentence(nbest.get(0));
                    if (currentSentence.get(ANCHOR_START) != mStart){
                        // only display backword sentence
                        //Log.d(TAG, "onPartialResults: "+ currentSentence.get(ANCHOR_START));
                        mBuilder.setSpan(mHighlightPotential,currentSentence.get(ANCHOR_START),
                                            currentSentence.get(ANCHOR_END), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        mSpeechText.setText(mBuilder);
                    }
                }
            }

            @Override
            public void onEvent(int eventType, Bundle params) { }
        });
    }

    /** get customer speech settings */
    private void setupCustomerSetting(){
        mSettingHelper = new SettingHelper(this);
        settings = mSettingHelper.getSetting();
        // setting customer page direction
        if (settings.get(USEDHAND).equals("Right")){
            mSpeechLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }else {
            mSpeechLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
        mTextSize = Integer.parseInt(settings.get(TEXTSIZE));       // setting customer text size
        mSpeechText.setTextSize(mTextSize);
    }

    /** get point speech from database by it's id */
    private void setupSpeech(){
        Intent intent = getIntent();
        if (intent != null && intent.getIntExtra(SPEECH_ID, 0) != 0) {
            mFullTextSpeech = SpeechDatabaseHelper.getSpeech(mSpeechDatabase,
                    intent.getIntExtra(SPEECH_ID, 0));
            // get data from FulltextSpeech
            mBuilder = new SpannableStringBuilder();
            mBuilder.append(KnifeParser.fromHtml(mFullTextSpeech.getSpeechDoc()));
            KnifeText.switchToKnifeStyle_(mBuilder, 0, mBuilder.length());
            mSpeechText.setText(mBuilder);
            mTotalTime = 0;
            //TODO change to keyword
            mKeywordTv.setText(mFullTextSpeech.getSpeechTitle());
        }
    }

    /** setup control box of it's start pause or back */
    @SuppressLint("HandlerLeak")
    private void setupControlBox(){
        // start speech
        mPageStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mControlBox.showNext();
                startTime();
                isPause = false;
                promptSpeechInput();
            }
        });
        // cancel and back to main page
        mPageEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // pause the speech
        mTimerPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO-----------------------------------ddddddd
                // remove all timer handles in backgrond
                if (mTimer != null) {
                    mTimer.cancel();
                    mTimer = null;
                }
                if (mTimeHandler != null) {
                    mTimeHandler.removeMessages(0);
                    mTimeHandler = null;
                }
                if (mRecognizer != null){
                    isPause = true;
                    mRecognizer.cancel();
                }
                mControlBox.showNext();
            }
        });
    }

    /**
     *  use intent to ask result from google
     */
    private void promptSpeechInput() {
        Log.d(TAG, "promptSpeechInput: ");
        isListening = true;
        Intent speechIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH); // Locale.getDefault()
        speechIntent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "start");
        speechIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        if (!speechIntent.hasExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE))
        {
            speechIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "com.mad.palmtest");
        }
        mRecognizer.startListening(speechIntent);
    }

    /**
     * to dynamicly get nesessary perimisions
     */
    private void getPermission(){
        ArrayList<String> toApplyList = new ArrayList<String>();
        for (String perm : PERMISIONS) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, perm)) {
                toApplyList.add(perm);
                Log.e(TAG, "no permission: " +perm);
            }
        }
        String tmpList[] = new String[toApplyList.size()];
        if (!toApplyList.isEmpty()) {
            ActivityCompat.requestPermissions(this, toApplyList.toArray(tmpList), 123);
        }
    }

    /* the timer for calculate time */
    @SuppressLint("HandlerLeak")
    private void startTime() {
        if (mTimer == null) {
            mTimer = new Timer();
        }
        if (mTimeHandler == null){
            // handle the calculation of timer, change current time status
            mTimeHandler = new Handler() {
                public void handleMessage(android.os.Message msg) {
                    mTimerTotalTv.setText(DateFormater.stringForTime(msg.arg1 * 10));
                    startTime();
                }
            };
        }
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                mTotalTime++;
                Message message = Message.obtain();
                message.arg1 = mTotalTime;
                // notis handle to change time status
                mTimeHandler.sendMessage(message);
            }
        };
        mTimer.schedule(mTimerTask, 10);
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRecognizer.destroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * prepare the anchors use to tracing speech
     * must after get speech data
     */
    private class initSpeechAnchors extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            Log.d(TAG, "doInBackground: ");
            mAnchorHelper.initAnchor(mBuilder.toString());
            return null;
        }
        /* hidden the view, and display loading process */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mControlBox.setVisibility(View.GONE);
            mLoadingAnchors.setVisibility(View.VISIBLE);
        }

        /*  hidden the loading process, and display the view */
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mLoadingAnchors.setVisibility(View.GONE);
            mControlBox.setVisibility(View.VISIBLE);
        }
    }


    public static String getErrorMsg(int error) {
        switch (error) {
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                return "Network operation timed out.";
            case SpeechRecognizer.ERROR_NETWORK:
                return "Other network related errors.";
            case SpeechRecognizer.ERROR_AUDIO:
                return "Audio recording error.";
            case SpeechRecognizer.ERROR_SERVER:
                return "Server sends error status.";
            case SpeechRecognizer.ERROR_CLIENT:
                return "Other client side errors.";
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                return "No speech input.";
            case SpeechRecognizer.ERROR_NO_MATCH:
                return "No recognition result matched.";
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                return "RecognitionService busy.";
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                return "Insufficient permissions.";
            default:
                return "Unknown error.";
        }
    }
}
