package com.mad.palmtest.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.mad.palmtest.R;
import com.mad.palmtest.adapter.SpeechAdapter;
import com.mad.palmtest.javabean.SpeechsInfo;
import com.mad.palmtest.datamanager.SpeechDatabaseHelper;

import java.util.ArrayList;

/**
 * The main menu display all speech document
 * allow user edit delete or statr speech
 *
 */
public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MAD_MAIN";

    private Toolbar toolbar;
    private ImageView mSettingIV;
    private FloatingActionButton mAddNewSpeech;
    private RecyclerView mSpeechRecyc;
    private SpeechAdapter mSpeechAdapter;
    private ProgressBar mLoadingPgBar;

    private SQLiteOpenHelper mSpeechDbHelper;                                    // for help get database, and do some actions to database
    private SQLiteDatabase mSpeechDatabase;                                      // speech's database

    private ArrayList<SpeechsInfo> mSpeechsList;
    private SearchView mSearchBar;

    private AlertDialog.Builder mBuilder;
    private AlertDialog mPopDialog;
    private View popWin;
    ImageView mFulltextImg, mPointImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAddNewSpeech = (FloatingActionButton) findViewById(R.id.fab);
        mSpeechRecyc = (RecyclerView) findViewById(R.id.speechsinfo_recyc);
        mLoadingPgBar = (ProgressBar)findViewById(R.id.main_loading_progress);
        mSearchBar = findViewById(R.id.speech_search_bar);
        mSettingIV = findViewById(R.id.speech_setting);

        /* set up for get database */
        mSpeechDbHelper = SpeechDatabaseHelper.getInstance(MainActivity.this.getApplicationContext());
        mSpeechDatabase = mSpeechDbHelper.getWritableDatabase();
        mSpeechsList = SpeechDatabaseHelper.getAllSpeechInfo(mSpeechDatabase);

        /* set up for recyc view by adapter */
        mSpeechAdapter = new SpeechAdapter(this.mSpeechsList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mSpeechRecyc.setLayoutManager(mLayoutManager);
        mSpeechRecyc.setItemAnimator(new DefaultItemAnimator());
        mSpeechRecyc.setAdapter(mSpeechAdapter);
        mSpeechAdapter.notifyDataSetChanged();


        /* set up search bar */
        EditText searchText = mSearchBar.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setTextColor(Color.GRAY);           // set text color
        searchText.setHintTextColor(getResources().getColor(R.color.colorPrimaryDark));
        mSearchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.isEmpty()){
                    Toast.makeText(MainActivity.this, "Please input Search Keyword！", Toast.LENGTH_SHORT).show();
                }else {
                    mSpeechAdapter.getFilter().filter(query);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()){
                    mSpeechAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });

        mSettingIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent setting = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(setting);
            }
        });


        /* pop window to choose which speech want to create */
        mAddNewSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popCreateNewSpeech();
            }
        });

    }

    /** pop dialog window of choose speech type and go to editor */
    private void popCreateNewSpeech(){
        mBuilder = new AlertDialog.Builder(this);
        popWin = LayoutInflater.from(this).inflate(R.layout.pop_create_newdoc, null);
        mBuilder.setView(popWin);
        mPopDialog = mBuilder.show();

        mFulltextImg = (ImageView)popWin.findViewById(R.id.pop_create_fulltext);
        mPointImg = (ImageView)popWin.findViewById(R.id.pop_create_point);

        mFulltextImg.setOnClickListener(new View.OnClickListener() {
            // create Full text speech document
            @Override
            public void onClick(View v) {
                Intent newSpeech = new Intent(MainActivity.this, SpeechEditActivity.class);
                mPopDialog.dismiss();
                startActivity(newSpeech);

            }
        });

        mPointImg.setOnClickListener(new View.OnClickListener() {
            // create point speech document
            @Override
            public void onClick(View v) {
                Intent newSpeech = new Intent(MainActivity.this, PointEditActivity.class);
                mPopDialog.dismiss();
                startActivity(newSpeech);
            }
        });

    }


    /*Called when activity becomes visible to the user*/
    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
    }

    /*Called when activity has been stopped and is restarting again*/
    @Override
    protected void onRestart() {
        Log.d(TAG, "onRestart: ");
        super.onRestart();

    }

    /*Called when activity starts interacting with user*/
    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
        // refresh list if there is any update
        new RefreshSpeechInfoList().execute();
    }

    /*Called when current activity is being paused and the previous activity is being resumed*/
    @Override
    protected void onPause() {
        Log.d(TAG, "onPause: ");
        super.onPause();
    }

    /*Called when activity is no longer visible to user*/
    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
    }

    /*Called before the activity is destroyed by the system (either manually or by the system to conserve memory) */
    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }

    /**
     * refresh the speechs Info in list
     */
    private class RefreshSpeechInfoList extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            Log.d(TAG, "doInBackground: ");
            // clear old list and get new one from database
            mSpeechsList.clear();
            mSpeechsList.addAll((ArrayList<SpeechsInfo>) SpeechDatabaseHelper.getAllSpeechInfo(mSpeechDatabase));
            return null;
        }
        /* hidden the recyc view, and display loading process */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSpeechRecyc.setVisibility(View.GONE);
            mLoadingPgBar.setVisibility(View.VISIBLE);

        }

        /* refresh adapter, hidden the loading process, and display the recyc view */
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mSpeechAdapter.notifyDataSetChanged();
            mLoadingPgBar.setVisibility(View.GONE);
            mSpeechRecyc.setVisibility(View.VISIBLE);
        }
    }



}
