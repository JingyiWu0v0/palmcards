package com.mad.palmtest.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mad.palmtest.R;
import com.mad.palmtest.datamanager.SettingHelper;

import java.util.Map;

/**
 * the activity for user setting customner setting
 *
 */
public class SettingActivity extends AppCompatActivity {
    private final static String TAG = "MAD_SET";
    private final static String USEDHAND = "used_hand";
    private final static String TEXTSIZE = "text_size";

    private String mUsedHand;
    private int mTextSize;
    private Switch mUsedHandSw;
    private EditText mTextSizeEt;
    private TextView mTextSizeExmpTv;
    private Button mSaveBtn;
    private Toolbar toolbar;

    SettingHelper mSettingHelper;           // the helper for get or save setting
    Map<String,String> settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        toolbar = findViewById(R.id.toolbar);

        mUsedHandSw = findViewById(R.id.set_used_hand);
        mTextSizeEt = findViewById(R.id.set_text_size);
        mTextSizeExmpTv = findViewById(R.id.set_text_size_exp);
        mSaveBtn = findViewById(R.id.set_save);

        /* set up tool bar */
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();       // back to main page
            }
        });
        setup();

    }
    /* set up this  page */
    private void setup(){
        /* get current settings */
        mSettingHelper = new SettingHelper(this);
        settings = mSettingHelper.getSetting();

        mUsedHand = settings.get(USEDHAND);
        mUsedHandSw.setText(mUsedHand);
        if (mUsedHand.equals("Right")){
            mUsedHandSw.setChecked(true);
        }else {
            mUsedHandSw.setChecked(false);
        }
        mTextSize = Integer.parseInt(settings.get(TEXTSIZE));
        mTextSizeEt.setText(String.valueOf(mTextSize));
        mTextSizeExmpTv.setTextSize(TypedValue.COMPLEX_UNIT_SP , mTextSize);

        /* change example text after input is changed */
        mTextSizeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mTextSizeEt.getText().toString().equals("")){
                mTextSize = Integer.parseInt(mTextSizeEt.getText().toString());
                if (mTextSize >30 || mTextSize<1){
                    mTextSize = 30;

                }
                mTextSizeExmpTv.setTextSize(TypedValue.COMPLEX_UNIT_SP , mTextSize);}
            }
        });
        /* change text when used hand switch is changed */
        mUsedHandSw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "onCheckedChanged: ");
                if (isChecked){
                    mUsedHand = "Right";
                }else {
                    mUsedHand = "Left";
                }
                mUsedHandSw.setText(mUsedHand);
            }
        });
        /* save all settings and show message */
        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SettingActivity.this,getResources().getString(R.string.set_toast_saved),
                        Toast.LENGTH_LONG).show();
                mSettingHelper.saveSetting(mUsedHand, String.valueOf(mTextSize) );
            }
        });

    }

}
