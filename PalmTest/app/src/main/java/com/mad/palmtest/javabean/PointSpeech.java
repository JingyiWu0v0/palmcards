package com.mad.palmtest.javabean;

import java.util.ArrayList;
import java.util.Date;

/**
 * carry point speech info
 * help when read or write it in database
 *
 */
public class PointSpeech {
    private int speechId;
    private Date speechDate;
    private String speechTitle;
    private ArrayList<String> pageTitles;
    private ArrayList<String> pages;
    private ArrayList<Integer> pageTimes;

    public PointSpeech(int speechId, Date speechDate, String speechTitle, ArrayList<String> pageTitles,
                       ArrayList<String> pages, ArrayList<Integer> pageTimes) {
        this.speechId = speechId;
        this.speechDate = speechDate;
        this.speechTitle = speechTitle;
        this.pageTitles = pageTitles;
        this.pages = pages;
        this.pageTimes = pageTimes;
    }

    public PointSpeech(int speechId, Date speechDate, String speechTitle) {
        this.speechId = speechId;
        this.speechDate = speechDate;
        this.speechTitle = speechTitle;
        this.pageTitles = new ArrayList<>();
        this.pages = new ArrayList<>();
        this.pageTimes = new ArrayList<>();
    }

    public int getSpeechId() {
        return speechId;
    }

    public void setSpeechId(int speechId) {
        this.speechId = speechId;
    }

    public Date getSpeechDate() {
        return speechDate;
    }

    public void setSpeechDate(Date speechDate) {
        this.speechDate = speechDate;
    }

    public String getSpeechTitle() {
        return speechTitle;
    }

    public void setSpeechTitle(String speechTitle) {
        this.speechTitle = speechTitle;
    }

    // page
    public ArrayList<String> getPageTitles() {
        return pageTitles;
    }

    public void setPageTitles(ArrayList<String> pageTitles) {
        this.pageTitles = pageTitles;
    }
    public void addPageTitles(String pageTitles) {
        this.pageTitles.add(pageTitles);
    }

    public ArrayList<String> getPages() {
        return pages;
    }

    public void setPages(ArrayList<String> pages) {
        this.pages = pages;
    }

    public void addPages(String pages) {
        this.pages.add(pages);
    }

    public ArrayList<Integer> getPageTimes() {
        return pageTimes;
    }

    public void setPageTimes(ArrayList<Integer> pageTimes) {
        this.pageTimes = pageTimes;
    }

    public void addPageTimes(Integer pageTimes) {
        this.pageTimes.add(pageTimes);
    }

    public int getSize() {
        return pageTitles.size();
    }
}
