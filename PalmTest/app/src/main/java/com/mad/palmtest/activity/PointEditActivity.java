package com.mad.palmtest.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mad.palmtest.DateFormater;
import com.mad.palmtest.R;
import com.mad.palmtest.javabean.PointSpeech;
import com.mad.palmtest.knife.KnifeText;
import com.mad.palmtest.datamanager.SpeechDatabaseHelper;

import java.util.ArrayList;
import java.util.Date;

/**
 * the editor of point speech
 * allow user write speech in different pages
 */
public class PointEditActivity extends AppCompatActivity {
    private final static String TAG = "MAD_EDIT_POINT";
    private static final String SPEECH_ID = "speech_id";

    private ImageButton mToolUndo, mToolRedo, mToolHighlight, mToolBold, mToolItalic,
            mToolUnderline, mToolLink, mToolBullet, mToolStrikethrough;
    private Button mLastPageBtn, mNextPageBtn;
    private EditText mPageTitleEt;
    private EditText mPageTimeEt;
    private TextView mPageNum;
    private Toolbar mToolbar;

    private KnifeText mSpeechEditor;

    private SQLiteOpenHelper mSpeechDbHelper;                                    // for help get database, and do some actions to database
    private SQLiteDatabase mSpeechDatabase;                                      // speech's database

    private PointSpeech mPointSpeech;

    private int mYear, mMonth, mDay;

    private ArrayList<String> mPageTitles;
    private ArrayList<String> mPageDocs;
    private ArrayList<Integer> mPageTimes;
    private static int sPageNum;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_edit);

        mToolUndo       = (ImageButton)findViewById(R.id.tool_undo_btn);
        mToolRedo       = (ImageButton)findViewById(R.id.tool_redo_btn);
        mToolHighlight  = (ImageButton)findViewById(R.id.tool_highlight_btn);
        mToolBold       = (ImageButton)findViewById(R.id.tool_bold_btn);
        mToolItalic     = (ImageButton) findViewById(R.id.tool_italic_btn);
        mToolUnderline  = (ImageButton) findViewById(R.id.tool_underline_btn);
        mToolStrikethrough = (ImageButton)findViewById(R.id.tool_strikethrough_btn);
        mToolBullet     = (ImageButton)findViewById(R.id.tool_bullet_btn);
        mToolLink       = (ImageButton)findViewById(R.id.tool_link_btn);

        mLastPageBtn    = (Button)findViewById(R.id.edit_last_page);
        mNextPageBtn    = (Button)findViewById(R.id.edit_next_page);
        mPageTitleEt    = (EditText) findViewById(R.id.edit_current_page_title);
        mPageTimeEt     = (EditText) findViewById(R.id.edit_speech_time);
        mPageNum        = (TextView) findViewById(R.id.edit_current_page);

        mSpeechEditor = (KnifeText) findViewById(R.id.speech_editor);

        /* set up tool bar */
        mToolbar = (Toolbar) findViewById(R.id.point_editor_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /* set up for get database */
        mSpeechDbHelper = SpeechDatabaseHelper.getInstance(PointEditActivity.this.getApplicationContext());
        mSpeechDatabase = mSpeechDbHelper.getWritableDatabase();


        /** get point speech from database by it's id */
        Intent intent = getIntent();
        if (intent!= null && intent.getIntExtra(SPEECH_ID,0)!= 0){
            mPointSpeech = SpeechDatabaseHelper.getPointSpeech(mSpeechDatabase,
                    intent.getIntExtra(SPEECH_ID,0));

            // get data from pointSpeech
            mPageTitles = mPointSpeech.getPageTitles();
            mPageTimes = mPointSpeech.getPageTimes();
            mPageDocs = mPointSpeech.getPages();
            sPageNum = 0;
            setPage();

        }else {
            sPageNum= 0;
            mPageTitles = new ArrayList<>();
            mPageTimes  = new ArrayList<>();
            mPageDocs = new ArrayList<>();
            mSpeechEditor.setText("");
            mPageDocs.add("");
            mPageTitleEt.setText("");
            mPageTitles.add("");
            mPageTimeEt.setText("0");
            mPageTimes.add(0);
            mPageNum.setText(String.valueOf(sPageNum+1));
        }

        setUpToolComponents();
        setUpPageComponents();
    }

    /** set up page shift and perpoty components */
    @SuppressLint("SetTextI18n")
    private void setUpPageComponents(){
        /* move to last page */
        mLastPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sPageNum-1 >= 0){
                    savePage();
                    sPageNum--;
                    setPage();
                }
            }
        });
        /* move to next page */
        mNextPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sPageNum+1 < mPageTitles.size()){
                    // if have next page, then move
                    savePage();
                    sPageNum++;
                    setPage();
                }else {
                    // if is not existing next page, ask for create new page
                    new AlertDialog.Builder(PointEditActivity.this)
                            .setMessage(PointEditActivity.this.getResources().getString(R.string.warn_create_new_page))
                            .setPositiveButton(PointEditActivity.this.getResources().getString(R.string.st_yes),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //TODO
                                            savePage();
                                            sPageNum++;
                                            mSpeechEditor.setText("");
                                            mPageDocs.add("");
                                            mPageTitleEt.setText("");
                                            mPageTitles.add("");
                                            mPageTimeEt.setText("0");
                                            mPageTimes.add(0);
                                            mPageNum.setText(String.valueOf(sPageNum+1));
                                        }
                                    }).setNegativeButton(PointEditActivity.this.getResources().getString(R.string.st_cancel),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });

        Log.d(TAG, "setUpPageComponents: "+ mPageTimes.get(sPageNum));

    }
    /** set up the text edit tool's components */
    private void setUpToolComponents(){

        /** undo action*/
        mToolUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.undo();
            }
        });


        /** redo action*/
        mToolRedo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.redo();
            }
        });


        /** Add style of highlight to text  and set it is keyword */
        mToolHighlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.highlight(!mSpeechEditor.contains(KnifeText.FORMAT_HIGHLIGHT));
            }
        });

        /** Add style of bold to text*/
        mToolBold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.bold(!mSpeechEditor.contains(KnifeText.FORMAT_BOLD));
            }
        });

        /** Add style of italic to text*/
        mToolItalic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.italic(!mSpeechEditor.contains(KnifeText.FORMAT_ITALIC));
            }
        });

        /** Add style of underline to text*/
        mToolUnderline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.underline(!mSpeechEditor.contains(KnifeText.FORMAT_UNDERLINED));
            }
        });

        /** Add style of strike through to text*/
        mToolStrikethrough.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.strikethrough(!mSpeechEditor.contains(KnifeText.FORMAT_STRIKETHROUGH));
            }
        });

        /** Add style of bullet to text*/
        mToolBullet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.bullet(!mSpeechEditor.contains(KnifeText.FORMAT_BULLET));
            }
        });

        /** Add style of link to text*/
        mToolLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpeechEditor.underline(!mSpeechEditor.contains(KnifeText.FORMAT_LINK));
            }
        });


    }

    /** set up page by array data */
    @SuppressLint("SetTextI18n")
    private void setPage(){
        mPageTitleEt.setText(mPageTitles.get(sPageNum));
        mPageTimeEt.setText(mPageTimes.get(sPageNum).toString());
        mSpeechEditor.fromHtml(mPageDocs.get(sPageNum));
        mPageNum.setText(String.valueOf(sPageNum+1)+"/"+String.valueOf(mPageTitles.size()+"p"));
    }

    /** save current page to array */
    private void savePage(){
        mPageTitles.set(sPageNum, mPageTitleEt.getText().toString());
        mPageTimes.set(sPageNum, Integer.parseInt(mPageTimeEt.getText().toString()));
        mPageDocs.set(sPageNum,mSpeechEditor.toHtml() );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.speech_editor_toolbar, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_save) {
            saveSpeech();
            return true;
        }else if (id == R.id.menu_saveas){
            saveAsSpeech();
            return true;
        }else if (id == R.id.menu_clear){

            mSpeechEditor.clearFormats();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * quick save speech to database
     * if not existing, then save as new speech
     */
    private void saveSpeech(){
        if (mPointSpeech!=null){
            savePage();
            SpeechDatabaseHelper.savePointSpeech(mSpeechDatabase,mPointSpeech);
            return;
        }else saveAsSpeech();
    }

    /**
     * save speech as a new speech
     * pop the window for ask detail information
     */
    private void saveAsSpeech(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View popWin = LayoutInflater.from(this).inflate(R.layout.pop_saving_detail, null);
        builder.setView(popWin);
        final AlertDialog dialog = builder.show();
        final EditText mTitle = (EditText)popWin.findViewById(R.id.pop_save_title);
        final TextView mDate = (TextView)popWin.findViewById(R.id.pop_save_date);
        Button popSaveBtn = (Button)popWin.findViewById(R.id.pop_save_yes);
        Button popCancelBtn = (Button)popWin.findViewById(R.id.pop_save_cancel);

        /* set the speech date i want */
        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPointSpeech==null){
                    Date today = new Date(System.currentTimeMillis());
                    mYear   = today.getYear();
                    mMonth  = today.getMonth();
                    mDay    = today.getDay();
                }else {
                    Date date = mPointSpeech.getSpeechDate();
                    mYear   = date.getYear();
                    mMonth  = date.getMonth();
                    mDay    = date.getDay();
                }
                /* display menu to chose date */
                new DatePickerDialog(PointEditActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        setTitle((monthOfYear+1) + "-" + dayOfMonth);
                        mYear   = year;
                        mMonth  = monthOfYear;
                        mDay    = dayOfMonth;
                        mDate.setText((monthOfYear+1) +"-"+dayOfMonth);

                    }
                },mYear, mMonth,mDay).show();
            }
        });
        /* save the speech */
        popSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePage();
                mPointSpeech = new PointSpeech(0, DateFormater.intToDate(mYear, mMonth, mDay),mTitle.getText().toString());
                for(int i = 0; i< mPageTitles.size();i++){
                    mPointSpeech.addPageTitles(mPageTitles.get(i));
                    mPointSpeech.addPageTimes(mPageTimes.get(i));
                    mPointSpeech.addPages(mPageDocs.get(i));
                }
                SpeechDatabaseHelper.savePointSpeech(mSpeechDatabase,mPointSpeech);
                dialog.dismiss();
            }
        });
        /* cancel the saving */
        popCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    /*Called when activity becomes visible to the user*/
    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
    }

    /*Called when activity starts interacting with user*/
    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
        if(mPointSpeech!=null){
            mToolbar.setTitle(mPointSpeech.getSpeechTitle());
        }else {
            mToolbar.setTitle(getResources().getString(R.string.unname));
        }

    }

    /*Called when current activity is being paused and the previous activity is being resumed*/
    @Override
    protected void onPause() {
        Log.d(TAG, "onPause: ");
        super.onPause();
    }

    /*Called when activity is no longer visible to user*/
    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
    }

    /*Called before the activity is destroyed by the system (either manually or by the system to conserve memory) */
    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }



}
