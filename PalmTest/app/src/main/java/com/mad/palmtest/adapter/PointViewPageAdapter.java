package com.mad.palmtest.adapter;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ViewSwitcher;

import java.util.ArrayList;
import java.util.List;

/**
 * the adapter use on viewpager in point speech
 * take all views in list
 * and set or remove when page is swipe
 *
 */
public class PointViewPageAdapter extends PagerAdapter {
    private ArrayList<View> mPoints;    // view's list

    public PointViewPageAdapter(ArrayList<View> points) {
        super();
        mPoints = points;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = mPoints.get(position);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView(mPoints.get(position));
    }

    @Override
    public int getCount() {
        return mPoints.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
