package com.mad.palmtest.javabean;


/**
 * carry speech information
 * for display in main page
 */
public class SpeechsInfo {
    private int speechId;
    private String speechTitle;
    private String speechEditTime;
    private String speechTime;
    private String speechType;

    public SpeechsInfo(int speechId, String speechTitle, String speechEditTime, String speechTime, String speechType) {
        this.speechId = speechId;
        this.speechTitle = speechTitle;
        this.speechEditTime = speechEditTime;
        this.speechTime = speechTime;
        this.speechType = speechType;
    }

    public String getSpeechType() {
        return speechType;
    }

    public void setSpeechType(String speechType) {
        this.speechType = speechType;
    }

    public int getSpeechId() {
        return speechId;
    }

    public void setSpeechId(int speechId) {
        this.speechId = speechId;
    }

    public String getSpeechTitle() {
        return speechTitle;
    }

    public void setSpeechTitle(String speechTitle) {
        this.speechTitle = speechTitle;
    }

    public String getSpeechEditTime() {
        return speechEditTime;
    }

    public void setSpeechEditTime(String speechEditTime) {
        this.speechEditTime = speechEditTime;
    }

    public String getSpeechTime() {
        return speechTime;
    }

    public void setSpeechTime(String speechTime) {
        this.speechTime = speechTime;
    }
}
