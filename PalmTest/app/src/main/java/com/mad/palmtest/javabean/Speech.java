package com.mad.palmtest.javabean;

import java.util.Date;

/**
 * carry fulltext speech
 * helpe when read or write it in database
 */
public class Speech {
    private int speechId;
    private String speechTitle;
    private Date speechTime;
    private String speechDoc;

    public Speech(int speechId, String speechTitle, Date speechTime, String speechDoc) {
        this.speechId = speechId;
        this.speechTitle = speechTitle;
        this.speechTime = speechTime;
        this.speechDoc = speechDoc;
    }

    public int getSpeechId() {
        return speechId;
    }

    public void setSpeechId(int speechId) {
        this.speechId = speechId;
    }

    public String getSpeechTitle() {
        return speechTitle;
    }

    public void setSpeechTitle(String speechTitle) {
        this.speechTitle = speechTitle;
    }

    public Date getSpeechTime() {
        return speechTime;
    }

    public void setSpeechTime(Date speechTime) {
        this.speechTime = speechTime;
    }

    public String getSpeechDoc() {
        return speechDoc;
    }

    public void setSpeechDoc(String speechDoc) {
        this.speechDoc = speechDoc;
    }
}
