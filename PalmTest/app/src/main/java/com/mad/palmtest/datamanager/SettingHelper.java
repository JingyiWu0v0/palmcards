package com.mad.palmtest.datamanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * for save and get customer setting of speech
 *
 */
public class SettingHelper {
    private final static String TAG = "MAD_SET_HELP";
    private final static String SETTINGFILE = "sp_setting";
    private final static String USEDHAND = "used_hand";
    private final static String TEXTSIZE = "text_size";
    private Context mConext;

    public SettingHelper(Context conext) {
        this.mConext = conext;
    }

    /** save setting to file*/
    public void saveSetting(String usedhand, String textsize){
        Log.d(TAG, "saveSetting: "+ usedhand + textsize);
        SharedPreferences preferences = mConext.getSharedPreferences(SETTINGFILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USEDHAND, usedhand);
        editor.putString(TEXTSIZE, textsize);
        editor.apply();
    }
    /** get setting in map */
    public Map<String,String> getSetting(){
        Log.d(TAG, "getSetting: ");
        Map<String ,String > settings = new HashMap<String, String>();
        SharedPreferences preferences = mConext.getSharedPreferences(SETTINGFILE, Context.MODE_PRIVATE);
        settings.put(USEDHAND, preferences.getString(USEDHAND,"Right"));
        settings.put(TEXTSIZE, preferences.getString(TEXTSIZE,"30"));
        return settings;
    }

}
