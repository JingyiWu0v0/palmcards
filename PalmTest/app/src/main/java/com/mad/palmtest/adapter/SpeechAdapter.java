package com.mad.palmtest.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import com.mad.palmtest.R;
import com.mad.palmtest.activity.FulltextSpeechActivity;
import com.mad.palmtest.activity.PointEditActivity;
import com.mad.palmtest.activity.PointSpeechActivity;
import com.mad.palmtest.activity.SpeechEditActivity;
import com.mad.palmtest.javabean.Speech;
import com.mad.palmtest.javabean.SpeechsInfo;
import com.mad.palmtest.datamanager.SpeechDatabaseHelper;


import java.util.ArrayList;
import java.util.List;

import static android.support.v4.content.ContextCompat.startActivity;

/**
 * The revecler view adapter for display and open speechs in main activity
 *
 */
public class SpeechAdapter extends RecyclerView.Adapter<SpeechAdapter.ViewHolder> implements Filterable{
    private final static String TAG = "MAD_ADP";
    private static final String SPEECH_ID = "speech_id";
    private static final String TYPE_FULLTEXT = "fulltext";
    private static final String TYPE_POINT = "point";
    private Intent mySpeech;

    private ArrayList<SpeechsInfo> mSpeechInfo;
    private ArrayList<SpeechsInfo> mSearchList;
    private Context mContext;

    /**
     * create adapter by speech infomation from database
     * @param mSpeechInfo the list of SpeechsInfo take from database
     * @param mContext
     */
    public SpeechAdapter(ArrayList<SpeechsInfo> mSpeechInfo, Context mContext) {
        this.mSpeechInfo = mSpeechInfo;
        this.mSearchList = mSpeechInfo;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.speech_row, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: "+ position);
        final SpeechsInfo speechInfo = mSearchList.get(position);

        // set up text by data  of speech
        holder.mTitle.setText(speechInfo.getSpeechTitle());
        holder.mEditTime.setText(mContext.getResources().getString(R.string.sft_last_edit) + speechInfo.getSpeechEditTime());
        holder.mTime.setText(speechInfo.getSpeechTime());

        /* setup different icon by it's type */
        if (speechInfo.getSpeechType().equals(TYPE_FULLTEXT)){
            holder.mIcon.setImageResource(R.mipmap.ic_doc);
            holder.mIcon.setBackgroundColor(mContext.getResources().getColor(R.color.sptype_fulltext) );

        }else {
            holder.mIcon.setImageResource(R.mipmap.ic_poi);
            holder.mIcon.setBackgroundColor(mContext.getResources().getColor(R.color.sptype_point));
        }

        /* open speech by it's id */
        holder.mIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: "+ speechInfo.getSpeechTitle());

                if (speechInfo.getSpeechType().equals(TYPE_FULLTEXT)){
                    mySpeech = new Intent(mContext, FulltextSpeechActivity.class);
                }else {
                    mySpeech = new Intent(mContext, PointSpeechActivity.class);
                }

                mySpeech.putExtra(SPEECH_ID,speechInfo.getSpeechId());
                startActivity(mContext, mySpeech,null);
            }
        });
//        holder.mSpeechRow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "onClick: "+ speechInfo.getSpeechTitle());
//
//                if (speechInfo.getSpeechType().equals(TYPE_FULLTEXT)){
//                    mySpeech = new Intent(mContext, FulltextSpeechActivity.class);
//                }else {
//                    mySpeech = new Intent(mContext, PointSpeechActivity.class);
//                }
//
//                mySpeech.putExtra(SPEECH_ID,speechInfo.getSpeechId());
//                startActivity(mContext, mySpeech,null);
//            }
//        });

        /* edit different speech by it's id */
        holder.mEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: "+ speechInfo.getSpeechTitle());

                if (speechInfo.getSpeechType().equals(TYPE_FULLTEXT)){
                    mySpeech = new Intent(mContext, SpeechEditActivity.class);
                }else {
                    mySpeech = new Intent(mContext, PointEditActivity.class);
                }

                mySpeech.putExtra(SPEECH_ID,speechInfo.getSpeechId());
                startActivity(mContext, mySpeech,null);
            }
        });

        /* delete speech, pop the window to make chance */
        holder.mDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.silv_delete))
                        .setMessage(mContext.getResources().getString(R.string.warn_delete))
                        .setPositiveButton(mContext.getResources().getString(R.string.st_yes),
                        new DialogInterface.OnClickListener() {
                            // make sure want to delete
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SpeechDatabaseHelper.deleteSpeech(speechInfo.getSpeechId(),speechInfo.getSpeechType());
                                mSpeechInfo.remove(position);
                                notifyDataSetChanged();

                            }
                        }).setNegativeButton(mContext.getResources().getString(R.string.st_cancel),
                        new DialogInterface.OnClickListener() {
                            // cancel the pop window
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                }).show();
            }
        });
    }

    /* get the number of items in speech info list */
    @Override
    public int getItemCount() {
        return mSearchList.size();
    }

    /**
     * filt keyword for search speech
     *
     * @return the result list
     */
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String constraintStr = constraint.toString();
                if (constraintStr.isEmpty()){
                    mSearchList = mSpeechInfo;
                }else {
                    ArrayList<SpeechsInfo> filteredList = new ArrayList<>();
                    for (SpeechsInfo speechsInfo : mSpeechInfo){
                        if (speechsInfo.getSpeechTitle().toLowerCase().contains(constraintStr.toLowerCase())){
                            // searching ignore case differents
                            filteredList.add(speechsInfo);
                        }
                    }
                    mSearchList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mSearchList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mSearchList = (ArrayList<SpeechsInfo>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    /**
     * for holder row's view components
     */
    class ViewHolder extends RecyclerView.ViewHolder{
        TextView mTitle, mEditTime, mTime;
        View mSpeechRow;
        ImageView mIcon;
        Button mEditBtn, mDeleteBtn;

        ViewHolder(View itemView) {
            super(itemView);
            mSpeechRow = itemView.findViewById(R.id.speech_row_swipe);
            mTitle =(TextView) itemView.findViewById(R.id.speech_row_title);
            mEditTime =(TextView) itemView.findViewById(R.id.speech_row_edit_time);
            mTime =(TextView) itemView.findViewById(R.id.speech_row_time);
            mIcon = (ImageView)itemView.findViewById(R.id.speech_row_icon);
            mEditBtn = (Button)itemView.findViewById(R.id.speech_row_edit);
            mDeleteBtn = (Button)itemView.findViewById(R.id.speech_row_delete);

            itemView.setTag(this);
        }
    }



}
