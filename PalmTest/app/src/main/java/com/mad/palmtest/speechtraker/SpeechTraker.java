package com.mad.palmtest.speechtraker;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.Locale;

/**
 * the Speech listener for get speech tests
 */
public class SpeechTraker implements RecognitionListener {
    private final static String TAG = "MAD_TRAKER";
    private final static String UNSTABLE_TEXT = "android.speech.extra.UNSTABLE_TEXT";

    /* status of recognizer */
    public static final int STATUS_None = 0;
    public static final int STATUS_WaitingReady = 2;
    public static final int STATUS_Ready = 3;
    public static final int STATUS_Speaking = 4;
    public static final int STATUS_Recognition = 5;
    private int mStatus = STATUS_None;

    private Context mConext;
    private long mStartTime;

    private SpeechRecognizer mRecognizer;
    private static SpeechTraker sSpeechTraker;


    /**
     * to create speech traker, and make sure only one exist
     * @return instance of this class
     */
    public static synchronized SpeechTraker getInstance(Context context) {
        Log.d(TAG, "getInstance: ");

        // make sure only one instance exist
        if (sSpeechTraker == null) {
            sSpeechTraker = new SpeechTraker(context);
            Log.d(TAG, "getInstance: created");
        }
        return sSpeechTraker;
    }
    
    private SpeechTraker(Context mConext) {
        this.mConext = mConext;

    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.d(TAG, "onReadyForSpeech: ");
        mStatus = STATUS_Ready;

    }

    @Override
    public void onBeginningOfSpeech() {
        Log.d(TAG, "onBeginningOfSpeech: ");
        mStatus = STATUS_Speaking;
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        Log.d(TAG, "onRmsChanged: " + rmsdB);

    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.d(TAG, "onBufferReceived: " + buffer[0]);

    }

    @Override
    public void onEndOfSpeech() {
        Log.d(TAG, "onEndOfSpeech: ");
        mStatus = STATUS_Recognition;

    }

    @Override
    public void onError(int error) {
        Log.d(TAG, "onError: " + getErrorMsg(error));
        mStatus = STATUS_None;
    }

    /**
     * Result out
     *
     * @param results
     */
    @Override
    public void onResults(Bundle results) {
        Log.d(TAG, "onResults ------------------");
        ArrayList<String> nbest = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        if (nbest.size() > 0) {
            Log.d(TAG, "Result: "+ nbest.get(0));
        }

    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        Log.d(TAG, "onPartialResults ------------------");
        ArrayList<String> nbest = partialResults.getStringArrayList(UNSTABLE_TEXT);
        if (nbest.size() > 0) {
            Log.d(TAG, "onPartialResult: "+ nbest.get(0));
        }
    }

    @Override
    public void onEvent(int eventType, Bundle params) {
        Log.d(TAG, "onEvent: " + eventType);

    }

    public static String getErrorMsg(int error) {
        switch (error) {
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                return "Network operation timed out.";
            case SpeechRecognizer.ERROR_NETWORK:
                return "Other network related errors.";
            case SpeechRecognizer.ERROR_AUDIO:
                return "Audio recording error.";
            case SpeechRecognizer.ERROR_SERVER:
                return "Server sends error status.";
            case SpeechRecognizer.ERROR_CLIENT:
                return "Other client side errors.";
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                return "No speech input.";
            case SpeechRecognizer.ERROR_NO_MATCH:
                return "No recognition result matched.";
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                return "RecognitionService busy.";
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                return "Insufficient permissions.";
            default:
                return "Unknown error.";
        }
    }

    /* use intent to ask result from google */
    private void promptSpeechInput() {
        Log.d(TAG, "promptSpeechInput: ");
        mStartTime = System.currentTimeMillis();

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH); // Locale.getDefault()
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "start");
        intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        if (!intent.hasExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE))
        {
            intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "com.mad.palmtest");
        }
        mRecognizer.startListening(intent);
    }
}
