package com.mad.palmtest;

import android.annotation.SuppressLint;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;

public class DateFormater {
    private final static String TAG = "MAD_DATE";
    private final static String DATE_FORMAT = "yyyy-MM-dd";
    private final static String DATETIME_FORMAT = "yyyy-MM-dd hh:mm:ss";


    /**
     * Formate date type in DD-Mon style of String
     * @param date the date need to change to word string
     */
    public static String formatDate(Date date){
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat monFormat = new SimpleDateFormat("MM");

        // change month number to english word
        String fMonth = monFormat.format(date);
        switch (fMonth){
            case "01": fMonth = "Jan"; break;
            case "02": fMonth = "Feb"; break;
            case "03": fMonth = "Mar"; break;
            case "04": fMonth = "Apr"; break;
            case "05": fMonth = "May"; break;
            case "06": fMonth = "Jun"; break;
            case "07": fMonth = "Jul"; break;
            case "08": fMonth = "Aug"; break;
            case "09": fMonth = "Sep"; break;
            case "10": fMonth = "Oct"; break;
            case "11": fMonth = "Nov"; break;
            case "12": fMonth = "Dec"; break;
        }
        return fMonth + "-" + dayFormat.format(date);
    }

    /**
     * Formate date type in DD-Mon style of String
     * @param dateStr the date need to change to word string
     * @param datetime is this date tring include time?
     */
    @SuppressLint("SimpleDateFormat")
    public static String formatSQLDate(String dateStr, Boolean datetime ){
        return formatDate(toJavaDate(dateStr, datetime) );
    }

    /**
     * Formate date type in SQLite date type style of String
     * @param date the date need to change to word string
     */
    public static String toSQLDate(Date date){
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat  format = new SimpleDateFormat(DATE_FORMAT);
        return format.format(date);
    }

    /**
     * Formate date type in SQLite datetime type style of String
     * @param date the date need to change to word string
     */
    public static String toSQLDateTime(Date date){
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat  format = new SimpleDateFormat(DATETIME_FORMAT);
        return format.format(date);
    }

    /**
     * Formate SQL string of date in JAVA date type
     * @param dateStr the date need to change to Date type
     */
    @SuppressLint("SimpleDateFormat")
    public static Date toJavaDate(String dateStr, Boolean datetime){
        SimpleDateFormat  format;
        if(datetime){
            format = new SimpleDateFormat(DATETIME_FORMAT);
        }else {
            format = new SimpleDateFormat(DATE_FORMAT);
        }
        Date date = null;
        try {
            date = (Date) format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * Formate SQL string of date in JAVA date type
     */
    public static Date intToDate(int year, int month, int day){
        String dateStr = year + "-" + month + "-" + day;
        return toJavaDate(dateStr, false);
    }

    public static String stringForTime(int timeMs){
        int totalSeconds = timeMs/1000;
        int minSeconds = timeMs%100;
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds/60)%60;
        int hours = totalSeconds/3600;
        StringBuilder mFormatBuilder = new StringBuilder();
        Formatter mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        mFormatBuilder.setLength(0);

        return mFormatter.format("%d:%02d:%02d",minutes,seconds,minSeconds).toString();
    }


}
