package com.mad.palmtest.widget;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mad.palmtest.R;

/**
 * The View group for Scroll View in ViewPager
 * it fix the Conflict of them
 */
public class ScrollableTextView extends ScrollView {
    TextView speechText;

    public ScrollableTextView(Context context) {
        super(context);
        View.inflate(context, R.layout.scrollable_text, this);
        speechText = findViewById(R.id.speechText);
        setVerticalScrollBarEnabled(false);
    }

    /**
     * when set text on this UI, set text to inside one
     * @param text
     */
    public void setText(CharSequence text) {
        speechText.setText(text);
    }

    /**
     * when set text size on this UI, set text size to inside one
     * @param textSize
     */
    public void setTextSize(float textSize) {
        speechText.setTextSize(textSize);
    }
}
