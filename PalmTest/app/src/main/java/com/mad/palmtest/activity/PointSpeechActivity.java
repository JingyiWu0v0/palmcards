package com.mad.palmtest.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mad.palmtest.DateFormater;
import com.mad.palmtest.R;
import com.mad.palmtest.adapter.PageAdapter;
import com.mad.palmtest.adapter.PointViewPageAdapter;
import com.mad.palmtest.animation.PointPageTransfomer;
import com.mad.palmtest.javabean.PointSpeech;
import com.mad.palmtest.knife.KnifeParser;
import com.mad.palmtest.knife.KnifeText;
import com.mad.palmtest.datamanager.SettingHelper;
import com.mad.palmtest.datamanager.SpeechDatabaseHelper;
import com.mad.palmtest.widget.ScrollableTextView;

import java.util.ArrayList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * the point speech active page
 * can calculate the time for each page
 * and change page by viewpager
 *
 */
public class PointSpeechActivity extends AppCompatActivity {
    private final static String TAG = "MAD_POINT";
    private static final String SPEECH_ID = "speech_id";
    private final static String USEDHAND = "used_hand";
    private final static String TEXTSIZE = "text_size";

    //private KnifeText mSpeechText;
    private ViewPager mPageViewer;
    private TextView mTimerPageTv;
    private TextView mTimerTotalTv;
    private TextView mPageNumTv;
    private TextView mPageStart;
    private TextView mPageEnd;

    private Handler mTimeHandler;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private ImageButton mTimerPause;
    private static int mTotalTime;
    private static int mPageTime;
    private static int mPageTimeOrig;
    private static boolean sPageTimeLow;

    private LinearLayout mRightPageStart;
    private LinearLayout mTimerLayout;
    private RelativeLayout mPointSpeechLayout;

    private SQLiteOpenHelper mSpeechDbHelper;                                    // for help get database, and do some actions to database
    private SQLiteDatabase mSpeechDatabase;                                      // speech's database

    private ArrayList<String> mPageTitles;
    private ArrayList<String> mPageDocs;
    private ArrayList<Integer> mPageTimes;
    private static int sPageNum;

    private RecyclerView mPageMenu;
    private PageAdapter mPagesAdapter;
    private ArrayList<View> mPageViews;
    private ScrollableTextView mPageView;
    SettingHelper mSettingHelper;
    Map<String,String> settings;
    private int mTextSize;

    private PointSpeech mPointSpeech;

    @SuppressLint({"ClickableViewAccessibility", "HandlerLeak"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_speech);
        getWindow().setFlags(WindowManager.LayoutParams. FLAG_FULLSCREEN ,WindowManager.LayoutParams. FLAG_FULLSCREEN);

        mPointSpeechLayout = findViewById(R.id.point_speech_act);
        mPageViewer = findViewById(R.id.dem_speech_viewpage);
        mTimerPageTv = findViewById(R.id.dem_timer_page);
        mTimerTotalTv = findViewById(R.id.dem_timer_total);
        mPageNumTv = findViewById(R.id.dem_page_num);

        mRightPageStart = findViewById(R.id.dem_right_ready);
        mPageStart = findViewById(R.id.dem_page_start);
        mPageEnd = findViewById(R.id.dem_page_end);
        mTimerLayout = findViewById(R.id.dem_timer_laylout);
        mTimerPause = findViewById(R.id.dem_timer_pause);
        mPageMenu = findViewById(R.id.dem_pages_menu);

        /* get speech settings */
        mSettingHelper = new SettingHelper(this);
        settings = mSettingHelper.getSetting();

        // setting customer page direction
        if (settings.get(USEDHAND).equals("Right")){
            mPointSpeechLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }else {
            mPointSpeechLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        // setting customer text size
        mTextSize = Integer.parseInt(settings.get(TEXTSIZE));

        /* set up for get database */
        mSpeechDbHelper = SpeechDatabaseHelper.getInstance(PointSpeechActivity.this.getApplicationContext());
        mSpeechDatabase = mSpeechDbHelper.getReadableDatabase();

        /** get point speech from database by it's id */
        Intent intent = getIntent();
        if (intent != null && intent.getIntExtra(SPEECH_ID, 0) != 0) {
            mPointSpeech = SpeechDatabaseHelper.getPointSpeech(mSpeechDatabase,
                    intent.getIntExtra(SPEECH_ID, 0));

            // get data from pointSpeech
            mPageTitles = mPointSpeech.getPageTitles();
            mPageTimes = mPointSpeech.getPageTimes();
            mPageDocs = mPointSpeech.getPages();
            sPageNum = 0;
            /* get doc's data and insert into pages */
            mPageViews = new ArrayList<>();
            for (int i = 0; i < mPageTitles.size(); i++) {
                SpannableStringBuilder builder = new SpannableStringBuilder();
                builder.append(KnifeParser.fromHtml(mPageDocs.get(i)));
                KnifeText.switchToKnifeStyle_(builder, 0, builder.length());
                mPageView = new ScrollableTextView(this);
                mPageView.setText(builder);
                mPageView.setTextSize(mTextSize);
                mPageViews.add(mPageView);
            }
            setPage();  // show first page
            mTimerTotalTv.setText(DateFormater.stringForTime(mTotalTime));
        }

        /* show pager from adapter */
        mPageViewer.setAdapter(new PointViewPageAdapter(mPageViews));
        mPageViewer.setPageTransformer(true, new PointPageTransfomer());
        mPageViewer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d(TAG, "onPageScrolled: ");
                sPageNum = position;
                setPage();      // when page changed, change control box also
                mPagesAdapter.setCurrentPage(position);
            }

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected: ");
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.d(TAG, "onPageScrollStateChanged: ");

            }
        });
        setComponent();

    }

    /** setup all components*/
    @SuppressLint({"ClickableViewAccessibility", "HandlerLeak"})
    private void setComponent() {
        /* switch control box status */
        mPageStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewFlipper) findViewById(R.id.timerViewSwitcher)).showNext();
                startTime();
            }
        });
        /* back to main */
        mPageEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /* switch control box statu */
        mTimerPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: timePause");
                ((ViewFlipper) findViewById(R.id.timerViewSwitcher)).showNext();
                /* remove all handles in backgrond */
                if (mTimer != null) {
                    mTimer.cancel();
                    mTimer = null;
                }
                if (mTimeHandler != null) {
                    mTimeHandler.removeMessages(0);
                    mTimeHandler = null;
                }
            }
        });


        /* pages adaper*/
        mPagesAdapter = new PageAdapter(mPageTitles,this);
        mPageMenu.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mPageMenu.setItemAnimator(new DefaultItemAnimator());
        mPageMenu.setAdapter(mPagesAdapter);
        mPagesAdapter.notifyDataSetChanged();
        mPagesAdapter.setOnItemClickListener(new PageAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                mPageViewer.setCurrentItem(position);
                mPagesAdapter.notifyDataSetChanged();
            }
        });
    }

    /** setup current page info in control box */
    @SuppressLint("SetTextI18n")
    private void setPage() {
        mPageTimeOrig = mPageTimes.get(sPageNum) * 60 * 100;
        mPageTime = mPageTimeOrig;
        sPageTimeLow = false;
        mTimerPageTv.setText(DateFormater.stringForTime(mPageTime * 10));
        mTimerPageTv.setTextColor(getResources().getColor(R.color.enough_time));
        //mSpeechText.fromHtml(mPageDocs.get(sPageNum));
        mPageNumTv.setText("P. " + String.valueOf(sPageNum + 1) + "/" + String.valueOf(mPageTitles.size()));

    }

    /* the timer for calculate time */
    @SuppressLint("HandlerLeak")
    private void startTime() {
        if (mTimer == null) {
            mTimer = new Timer();
        }if (mTimeHandler == null) {
            // handle the calculation of timer, change current time status
            mTimeHandler = new Handler() {
                public void handleMessage(android.os.Message msg) {
                    mTimerTotalTv.setText(DateFormater.stringForTime(msg.arg1 * 10));
                    mTimerPageTv.setText(DateFormater.stringForTime(msg.arg2 * 10));
                    if (msg.arg2 < mPageTimeOrig/5 && !sPageTimeLow){
                        mTimerPageTv.setTextColor(getResources().getColor(R.color.low_time));
                        sPageTimeLow = true;
                    }
                    startTime();
                }
            };
        }
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                mPageTime--;
                mTotalTime++;
                Message message = Message.obtain();
                message.arg1 = mTotalTime;
                message.arg2 = mPageTime;
                // notis handle to change time status
                mTimeHandler.sendMessage(message);
            }
        };
        mTimer.schedule(mTimerTask, 10);
    }

    /*Called when activity becomes visible to the user*/
    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
    }

    /*Called when activity starts interacting with user*/
    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
    }

    /*Called when current activity is being paused and the previous activity is being resumed*/
    @Override
    protected void onPause() {
        Log.d(TAG, "onPause: ");
        super.onPause();
    }

    /*Called when activity is no longer visible to user*/
    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
    }

    /* Called before the activity is destroyed by the system (either manually or by the system to conserve memory) */
    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
        /* remove all handles in backgrond */
        if (mTimer != null) {
            mTimer.cancel();
        }
        if (mTimeHandler != null) {
            mTimeHandler.removeMessages(0);
        }
    }
}
