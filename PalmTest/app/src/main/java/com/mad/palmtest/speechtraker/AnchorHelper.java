package com.mad.palmtest.speechtraker;

import android.content.Context;
import android.util.Log;
import android.util.Pair;
//import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

/**
 * Get documents word to translate to anchors
 * anchors will help to track speech
 *
 */
public class AnchorHelper {
    private final static String TAG  = "MAD_ANCHOR";
    private final static String ANCHOR_START  = "start";
    private final static String ANCHOR_END  = "end";
    private final static String REGULAR_P_PUNC = "[\\pP\\p{Punct}]";
    private final static String REGULAR_P = "[\\pP]";
    private final static String REGULAR_PUNC = "[\\p{Punct}]";

    private final static String[] PUNCLIST = new String[]{".", ",", "|", "'", String.valueOf('"'), "[", "]","(",")","<",">","#","*","^","%","!","@","$","%","^","&",";",":"};
    private final static List<String> SMALLIST = Arrays.asList("","is","are","a","an","the", "he","and","to","you","of",
            "she","him","her","my","i","that","this","there","their","those","then","them");

    private ArrayList<WordAnchor> mWordAnchorList;
    private ArrayList<WordAnchor> mSentenceAnchorList;

    //private HashMap<String,ArrayList<Integer>> mWordAnchorMap;
    private HashMap<String,ArrayList<Pair<Integer,Integer>>> mWordAnchorMap;    //[Keyword,[start, sentence index]]
    private HashMap<Integer, Integer> mSentenceAnchorMap;
    private ArrayList<Pair<Integer, Integer>> mSentenceAnchorArray;             //[start,end]

    private static AnchorHelper sInstance;
    private Context mContext;
    private String mDoc;

    private int mLastSentAnchor = 0;
    private int mWordAnchor = 0;

    private int potentialStart, currentStart = 0;
    private int potentialEnd, currentEnd = 1;
    private Map<String, Integer > current;
    private boolean mGuessWorking = false;

    private int combCount = 0;

    /**
     * to create speech anchors, and make sure only one exist
     * @return instance of this class
     */
    public static synchronized AnchorHelper getInstance(Context context) {
        Log.d(TAG, "getInstance: ");

        // make sure only one instance exist
        if (sInstance == null) {
            sInstance = new AnchorHelper(context);
            Log.d(TAG, "getInstance: created");
        }
        return sInstance;
    }
    private AnchorHelper(Context context) {
        this.mContext = context;
    }

    /**
     * init speech anchordatas
     * @param text document need listen
     */
    public void initAnchor(String text){
        mDoc = text;
        makeSentenceArray(mDoc);
        makeAnchorMap(mDoc);
        mLastSentAnchor = 0;

        current = new HashMap<String, Integer>();
        current.put(ANCHOR_START,potentialStart);
        current.put(ANCHOR_END,potentialEnd);
    }

    /**
     * guess current sentence
     * @param text
     * @return
     */
    public Map<String, Integer> getCurrentSentence(String text) {
        String[] sentences = text.split(" ");
        if (sentences.length <5) return current;
        int result = guessStence(sentences);
        if (result != -1){
            mLastSentAnchor = result;
            mWordAnchor = mSentenceAnchorArray.get(result).second;
            current = new HashMap<String, Integer>();
            currentStart = mSentenceAnchorArray.get(result).first;
            currentEnd = mSentenceAnchorArray.get(result).second;
            current.put(ANCHOR_START, currentStart);
            current.put(ANCHOR_END, currentEnd);
        }
        return current;
    }

    /**
     * guess potential current sentence
     * @param text
     * @return
     */
    public Map<String, Integer> getPotentialSentence(String text) {
        // make sure only one is working
        if (mGuessWorking == true) return current;
        mGuessWorking = true;
        String[] sentences = text.split(" ");
        if (sentences.length <1) return current;
        int result = guessStence(sentences);
        if (result != -1){
            if (mSentenceAnchorArray.get(result).first ==potentialStart){
                combCount ++;
            }else {
                potentialStart = mSentenceAnchorArray.get(result).first;
                potentialEnd = mSentenceAnchorArray.get(result).second;
                current = new HashMap<String, Integer>();
                current.put(ANCHOR_START,potentialStart);
                current.put(ANCHOR_END,potentialEnd);
            }
            if (combCount>2){
                mWordAnchor = potentialEnd;
                combCount = 0;
            }
        }
        mGuessWorking = false;
        return current;
    }

    /**
     * guess current sentence
     * @param sentences
     * @return the index of sentence in sentence array
     */
    private int guessStence(String[] sentences) {
        Map<Integer, ArrayList<Integer> > sentAnchors = new HashMap<Integer, ArrayList<Integer>>();
        Pair<Integer,Integer> currWord = new Pair<>(-1,-1);
        int anchor;
        int sentAnchor;
        for (String word : sentences) {
            // get the index of sentence of each word
            currWord = searchWordPosition(word,mWordAnchor);
            anchor = currWord.first;
            sentAnchor = currWord.second;
            if(sentAnchor ==-1) continue;
            else if (sentAnchors.containsKey(sentAnchor)){
                sentAnchors.get(sentAnchor).add(anchor);
            }else {
                sentAnchors.put(sentAnchor, new ArrayList<Integer>(anchor));
            }
        }
        // get the most accurate answer
        int maxAnchor = 0;
        int result = -1;
        for (Map.Entry<Integer, ArrayList<Integer>> entry : sentAnchors.entrySet()){
            if (entry.getValue().size() > maxAnchor){
                maxAnchor = entry.getValue().size();
                result = entry.getKey();
            }
        }
        return result;
    }

    /**
     * split each sentence into the sentence range set
     * @param text
     */
    private void makeSentenceArray(String text) {
        mSentenceAnchorArray = new ArrayList<>();
        Integer start = 0;
        Integer end = 0;
        String cut;
        String[] sentences = text.split("\\.");

        for (int i = 0; i< sentences.length ;i++){
            cut = sentences[i];
            end = start + cut.length();
            Pair<Integer, Integer> a = new Pair<>(start,end);
            mSentenceAnchorArray.add(a);
            start = end +1;
        }
    }

    /**
     * split all word in document into map
     * and with it's position of start, index of it's sentence
     * @param text speech document
     */
    private void makeAnchorMap(String text){
        mWordAnchorMap = new HashMap();
        //text = text.replaceAll(REGULAR_P," ");
        text = text.replaceAll(REGULAR_PUNC," ");
        StringBuilder builder;
        String cut;
        int start = 0;
        int end = 0;
        int position = 0;
        String[] words = text.split(" ");

        for (int i = 0; i< words.length ;i++){
            cut = words[i];
            end = start + cut.length();
            // deny some verb or not important word
            if (SMALLIST.contains(cut)){
                start = end +1;
                continue;
            }
            // add word's position to map
            position = searchWordInSentenceIndex(start);
            Pair<Integer, Integer> pair = new Pair<>(start,position);
            if(mWordAnchorMap.containsKey(cut)){
                mWordAnchorMap.get(cut).add(pair);
            }else{
                mWordAnchorMap.put(cut,new ArrayList<Pair<Integer, Integer>>());
                mWordAnchorMap.get(cut).add(pair);
            }
            start = end +1;
        }
    }


    /**
     * Find recognized word in word map
     * @param word the recognized word from speech
     * @param anchor the current anchor of word
     * @return the nearlest word info pair
     */
    private  Pair<Integer,Integer> searchWordPosition(String word, int anchor){
        Pair<Integer,Integer> result = new Pair<>(-1,-1);
        int start = 0;
        int end = 0;
        word = word.toLowerCase().trim();
        if (!mWordAnchorMap.containsKey(word))return result;

        int minDist = Integer.MAX_VALUE;
        int weight;
        Pair<Integer,Integer> currWord;

        for (Iterator<Pair<Integer,Integer>> iter = mWordAnchorMap.get(word).iterator();iter.hasNext();){
            currWord = iter.next();
            weight = currWord.first;
            if (weight>=anchor ){
                if  (Math.abs(anchor- weight)< minDist){
                    result = currWord;
                }
                break;
            }else {
                minDist = Math.abs(anchor- weight);
                result = currWord;
            }
        }
        Log.d(TAG, "searchWordPosition: "+ word + " result: "+ result);
        return result;
    }

    /* calculate distance, and also add distance to past words */
    private int getDistance(int anchor, int weight){
        int dist;
//        if (weight<anchor){
//            weight = weight -100;
//        }
        dist = Math.abs(anchor- weight);
        return dist;
    }


    /**
     * Binary Search word position in which sentence
     * @param pos the position of word start
     * @return the index of sentence carry this word;
     */
    private int searchWordInSentenceIndex(int pos){
        if (pos ==-1)return -1;
        int low = 0;
        int high = mSentenceAnchorArray.size();
        int mid = 0;
        int start = 0;
        int end = 0;
        while (low<=high){
            mid = (low +high)/2;
            start = mSentenceAnchorArray.get(mid).first;
            end = mSentenceAnchorArray.get(mid).second;
            if (pos >= start && pos<end){
                return mid;
            }else if (pos< start){
                high = mid-1;
            }else {
                low = mid+1;
            }
        }
        return -1;
    }
}

