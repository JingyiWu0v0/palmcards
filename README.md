## Palm Cat

This is an intelligent teleprompt application developed for UTS mobile application assigment 3 in 2018. 
The main innovation of this project is using the speech-recognize to tracking user speech, that makes the application can known where is user speaking and give right suggestion.


### The feature of this application includes:

1. Tracking your speech and highlight your speaking sentence on screen
2. The tip card allow you summarize keyword and sentences
3. Timer

in order to achieve the speeck recognize function, i used speed-to-text api built in google client (not chrome), 
so please make sure its open and have primention to microphone.


### Further development plan:

1. Synonym tracking
2. Proactive jumping button
3. PPT controler


